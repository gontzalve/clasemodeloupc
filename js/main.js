
window.onload = function(){

	
	let game = new Phaser.Game(1280,720,Phaser.AUTO,"content",
	{
		preload: load,
		create: create,
		update: update
	}); 

	
	// LOADING =====================================================

	function load(){
		loadWorld();
		loadDino();
	}

	function loadWorld(){
		game.load.image("background1", "assets/world/bg1.png");
		game.load.image("background2", "assets/world/bg2.png");
		game.load.image("ground1", "assets/world/ground1.png");
		game.load.image("ground2", "assets/world/ground2.png");
		game.load.image("ground3", "assets/world/ground3.png");
		game.load.image("ground4", "assets/world/ground4.png");
		game.load.image("ground5", "assets/world/ground5.png");
	}

	function loadDino(){
		game.load.image('dinoIdle', 'assets/dino/idle.png'); 
		game.load.image('goal', 'assets/dino/goal.png');
	}

	// CREATING =====================================================

	function create(){
		createWorld();
		createDino();
		createGoal();
	}

	function createWorld(){
		game.add.sprite(0,0,"background1");
		game.add.sprite(0,260,"background2");

		let grounds = game.add.group();

		grounds.create(-15,540, "ground4");
		grounds.create(400,540, "ground3");
		grounds.create(750,540, "ground1");
		grounds.create(1000,540, "ground4");
	}

	function createDino(){
		let dino = game.add.sprite(70, 540,"dinoIdle");
		dino.anchor.x = 0.5;
		dino.anchor.y = 1;
	}

	function createGoal(){
		let goal = game.add.sprite(1200,540,"goal");
		goal.anchor.x = 0.5;
		goal.anchor.y = 1;
	}

	// UPDATE =====================================================

	function update()
	{
		
	}
}